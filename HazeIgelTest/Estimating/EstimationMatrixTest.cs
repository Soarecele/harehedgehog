﻿using HazeIgel.Util;
using NUnit.Framework;

namespace HazeIgelTest.Estimating
{
    [TestFixture]
    public class EstimationMatrixTest
    {
        public EstimationMatrixTest()
        {
            ExecutablePath.InitializeFake(string.Empty);
        }

        [Test]
        public void Test()
        {
            var mtx = new EstimationMatrix(new []
            {
                new EstimationCoeffRange(0.5f, 2.0f, 15),
                new EstimationCoeffRange(0.5f, 2.0f, 15),
                new EstimationCoeffRange(0.5f, 2.0f, 15)
            });
            mtx.Calculate();
            var best = mtx.bestResults;
            var bestPercent = mtx.resultBest;
        }
    }
}
