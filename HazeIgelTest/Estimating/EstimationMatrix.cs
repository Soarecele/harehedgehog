﻿using System.Collections.Generic;
using System.Linq;
using HazeIgel.Model;
using HazeIgel.Model.AI;
using HazeIgel.Model.Estimation;

namespace HazeIgelTest.Estimating
{
    public class EstimationMatrix
    {
        public readonly EstimationCoeffRange[] coeffs;

        public readonly List<EstimationSample> samples;

        public EstimationResult[] results;

        public decimal resultBest, resultWorse, resultAvg;

        public List<EstimationResult> bestResults;

        private readonly FieldMapDescriptor desc;

        public EstimationMatrix(EstimationCoeffRange[] coeffs)
        {
            this.coeffs = coeffs;
            samples = new EstimationStore().GetSamples();
            results = new EstimationResult[coeffs[0].steps * coeffs[1].steps * coeffs[2].steps];
            PrepareResults();
            desc = new FieldMapDescriptor(FieldMap.Instance);
        }

        public void Calculate()
        {
            foreach (var sample in samples)
                ProcessSample(sample);
            SummarizeResults();
        }

        private void SummarizeResults()
        {
            resultBest = results.Max(r => r.HitPercent);
            resultWorse = results.Min(r => r.HitPercent);
            resultAvg = results.Average(r => r.HitPercent);
            var maxH = results.Max(r => r.hits);
            bestResults = results.Where(r => r.hits == maxH).ToList();
        }

        private void ProcessSample(EstimationSample sample)
        {
            var disp = new Disposition
            {
                hazes = sample.hazes
            };
            disp.UpdatePositions(FieldMap.Instance);
            var ctor = new EstimateCalculator(FieldMap.Instance, disp);

            for (var i = 0; i < results.Length; i++)
            {
                ctor.estimationCoeffs = results[i].coeffs;
                var pA = ctor.EstimatePosition(0);
                var pB = ctor.EstimatePosition(1);
                var hit = pA > pB && sample.winner == 1 || pA < pB && sample.winner == 2;
                results[i].hits += hit ? 1 : 0;
                results[i].misses += hit ? 0 : 1;
            }
        }

        private void PrepareResults()
        {
            var index = 0;
            for (var i = 0; i < coeffs[0].steps; i++)
                for (var j = 0; j < coeffs[1].steps; j++)
                    for (var k = 0; k < coeffs[2].steps; k++)
                        results[index++] = new EstimationResult(
                            new[]
                            {
                                coeffs[0].GetValueForStep(i),
                                coeffs[1].GetValueForStep(j),
                                coeffs[2].GetValueForStep(k)
                            });
        }
    }

    public class EstimationCoeffRange
    {
        public float min;

        public float max;

        public int steps;

        public EstimationCoeffRange()
        {
        }

        public EstimationCoeffRange(float min, float max, int steps)
        {
            this.min = min;
            this.max = max;
            this.steps = steps;
        }

        public float GetValueForStep(int step)
        {
            return min + (max - min) * step / steps;
        }

        public override string ToString()
        {
            return $"{min:f3} - {max:f3}, {steps} steps ({(max - min) / steps:f4})";
        }
    }

    public class EstimationResult
    {
        public float[] coeffs;

        public int hits;

        public int misses;

        public decimal HitPercent => hits * 100M / (hits + misses);

        public EstimationResult()
        {
        }

        public EstimationResult(float[] coeffs)
        {
            this.coeffs = coeffs;
        }

        public override string ToString()
        {
            return
                $"[{coeffs[0]:f2}, {coeffs[1]:f2}, {coeffs[2]:f2}] (S, C, P), {hits}h / {misses}m ({HitPercent:F1}%)";
        }
    }
}
