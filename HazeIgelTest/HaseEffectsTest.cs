﻿using HazeIgel.Model;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class HaseEffectsTest : TurnCheckerBaseTest
    {
        [Test]
        public void TestApplySpareCarrot()
        {
            // nothing to spare
            var disp = MakeDisposition(0,
                SpawnHaze(4, 29, 0, false),
                SpawnHaze(10, 4, 2, false));
            var checker = new TurnChecker(map, disp)
            {
                haseEffectKind = 0
            };
            var newDisp = checker.CheckTurnAndUpdate(2);
            Assert.IsNotNull(newDisp);
            Assert.IsNotEmpty(newDisp.haseEffects);
            Assert.AreEqual(29 - FieldMap.GetCarrotsPerMove(2), newDisp.hazes[0].carrots);

            // spare 10 to the loser
            disp = MakeDisposition(0,
                SpawnHaze(4, 29, 0, false),
                SpawnHaze(0, 4, 2, false));
            checker = new TurnChecker(map, disp)
            {
                haseEffectKind = 0
            };
            newDisp = checker.CheckTurnAndUpdate(2);
            Assert.AreEqual(29 - FieldMap.GetCarrotsPerMove(2) - 10, newDisp.hazes[0].carrots);
            Assert.AreEqual(4 + 10, newDisp.hazes[1].carrots);

            // the loser rejects
            disp = MakeDisposition(0,
                SpawnHaze(61, 29, 0, false),
                SpawnHaze(60, 80, 2, false));
            checker = new TurnChecker(map, disp)
            {
                haseEffectKind = 0
            };
            newDisp = checker.CheckTurnAndUpdate(2);
            Assert.AreEqual(29 - FieldMap.GetCarrotsPerMove(2), newDisp.hazes[0].carrots);
        }

        [Test]
        public void TestApplyFreeTurn()
        {
            // nothing to spare
            var disp = MakeDisposition(0,
                SpawnHaze(4, 29, 0, false),
                SpawnHaze(10, 4, 2, false));
            var checker = new TurnChecker(map, disp)
            {
                fixedHaseEffect = 11,
                haseEffectKind = TurnChecker.HaseCellEffect.FixedEffect
            };
            var newDisp = checker.CheckTurnAndUpdate(2);
            Assert.IsNotNull(newDisp);
            Assert.IsNotEmpty(newDisp.haseEffects);
            Assert.AreEqual(29, newDisp.hazes[0].carrots);
        }
    }
}
