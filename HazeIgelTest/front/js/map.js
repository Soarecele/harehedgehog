﻿function Map() {
    this.tableSelector = 'div#map-table';
    this.rows = 6;
    this.cols = 11;
    this.setupMapCells();
    this.setupCellCoords();
    this.setupTable();
    this.setupPlayerColors();
    this.onCellHover = null;
    this.onCellClick = null;
}

Map.prototype.highlightCells = function (indices, playerIndex) {
    var newClass = 'cell-marked-' + playerIndex;
    
    for (var i = 0; i < this.cells.length; i++) {
        var coords = this.celCoords[i];
        var cell = $(this.makeCellSelector(coords[1], coords[0]));
        var selected = indices[i];
        cell.removeClass(function (index, className) {
            return (className.match(/(^|\s)cell-marked\S*/g) || []).join(' ');
        });
        if (selected) {
            cell.addClass('cell-marked');
            cell.addClass(newClass);
        }
    }
}

Map.prototype.placePlayers = function (players) {
    for (var i = 0; i < this.cells.length; i++) {
        var coords = this.celCoords[i];
        var cell = $(this.makeCellSelector(coords[1], coords[0]) + ' div.cell-player');
        cell.html('');
    }
    for (var i = players.length - 1; i >= 0; i--) {
        var coords = this.celCoords[players[i].pos];
        var cell = $(this.makeCellSelector(coords[1], coords[0]) + ' div.cell-player');
        var img = this.getPlayerImage(players[i], i);
        cell.html('<img src="' + img + '" class="player-image" />');
    }
}

Map.prototype.getImgByCell = function(c) {
    var imgName = c.tp;
    if (c.tp === 'p')
        imgName += c.pos;
    return 'img/card_' + imgName + '.png';
}

Map.prototype.setupCellCoords = function() {
    this.celCoords = {};
    for (var i = 0; i <= 9; i++)
        this.celCoords[i] = [i + 1, 0];
    for (var i = 10; i <= 20; i++)
        this.celCoords[i] = [20 - i, 1];
    for (var i = 21; i <= 31; i++)
        this.celCoords[i] = [i - 21, 2];
    for (var i = 32; i <= 42; i++)
        this.celCoords[i] = [42 - i, 3];
    for (var i = 43; i <= 53; i++)
        this.celCoords[i] = [i - 43, 4];
    for (var i = 54; i <= 64; i++)
        this.celCoords[i] = [64 - i, 5];
}

Map.prototype.setupMapCells = function() {
    this.cells = [];
    this.cells[0] = { tp: 'start' };
    this.cells[64] = { tp: 'end' };

    for (var i = 1; i < 64; i++)
        this.cells[i] = { tp: 'c' };

    var haseIndx = [3, 6, 14, 25, 31, 34, 39, 46, 51, 58, 61, 63];
    for (var i = 0; i < haseIndx.length; i++)
        this.cells[haseIndx[i]] = { tp: 'h' };

    var igelIndx = [8, 11, 15, 19, 24, 30, 37, 43, 50, 56];
    for (var i = 0; i < igelIndx.length; i++)
        this.cells[igelIndx[i]] = { tp: 'i' };

    var saladIndx = [7, 22, 42, 57, 62];
    for (var i = 0; i < saladIndx.length; i++)
        this.cells[saladIndx[i]] = { tp: 's' };

    var p1indx = [16, 32, 48];
    for (var i = 0; i < p1indx.length; i++)
        this.cells[p1indx[i]] = { tp: 'p', pos: 1 };

    var p2indx = [10, 17, 23, 29, 35, 41, 47, 53, 60];
    for (var i = 0; i < p2indx.length; i++)
        this.cells[p2indx[i]] = { tp: 'p', pos: 2 };

    var p3indx = [4, 9, 12, 20, 28, 36, 44, 52];
    for (var i = 0; i < p3indx.length; i++)
        this.cells[p3indx[i]] = { tp: 'p', pos: 3 };

    var p4indx = [9, 18, 27, 45, 54];
    for (var i = 0; i < p4indx.length; i++)
        this.cells[p4indx[i]] = { tp: 'p', pos: 4 };
}

Map.prototype.setupTable = function() {
    var markup = '';
    var i = 0;
    for (var row = 0; row < this.rows; row++) {
        var rowMark = '<div class="rTableRow"> ';
        for (var col = 0; col < this.cols; col++) {
            var innerMark = '';
            if (i > 0) {
                innerMark = '<div class="cell-number">' +
                    '</div> <div class="cell-player"></div>';
            }
            rowMark += '<div class="rTableCell">' + innerMark + '</div> ';
            i++;
        }
        rowMark += '</div>\n';
        markup += rowMark;
    }
    $(this.tableSelector).html(markup);
    
    for (var i = 0; i < this.cells.length; i++) {
        var pos = this.celCoords[i];
        var td = $(this.makeCellSelector(pos[1], pos[0]));
        var img = this.getImgByCell(this.cells[i]);
        td.css('background-image', 'url(' + img + ')');
        td.css('background-repeat', 'no-repeat');
        td.find('div.cell-number').text(i + '');        
        td.hover(this.createHoverHandler(i));
        td.click(this.createClickHandler(i));
    }
}

Map.prototype.createHoverHandler = function (i) {
    var that = this;
    return function() {
        if (!that.onCellHover) return;
        that.onCellHover(i);
    }
}

Map.prototype.createClickHandler = function (i) {
    var that = this;
    return function () {
        if (!that.onCellClick) return;
        that.onCellClick(i);
    }
}

Map.prototype.getPlayerImage = function(p, index) {
    var suffix = p.waits ? 'w' : 'a';
    var preffix = 'player';
    if (this.computerPlayers &&
        this.computerPlayers.length > index &&
        this.computerPlayers[index])
        preffix = 'computer';

    return 'img/' + preffix + '_' + (index + 1) + '_' + suffix + ".png";
}

Map.prototype.setupPlayerColors = function() {
    this.playerColor = {};
    this.playerColor[0] = '#f200f2';
    this.playerColor[1] = '#6200f2';
    this.playerColor[2] = '#60ff30';
    this.playerColor[3] = '#e05000';
    this.playerColor[4] = '#e0e000';
    this.playerColor[5] = '#000000';
}

Map.prototype.makeCellSelector = function(row, col) {
    return this.tableSelector + ' div.rTableRow:nth-child(' + (row + 1) + ') div.rTableCell:nth-child(' + (col + 1) + ')';
}