﻿using System.Collections.Generic;
using System.Linq;
using HazeIgel.Model;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class FieldMapTest
    {
        [Test]
        public void TestInitMapIgels()
        {
            var map = FieldMap.Instance;
            var igelPos = new Dictionary<int, IgelCell>();
            for (var i = 0; i < map.cells.Length; i++)
                if (map.cells[i] is IgelCell)
                    igelPos.Add(i, (IgelCell) map.cells[i]);

            var igels = map.cells.Where(c => c is IgelCell).Cast<IgelCell>().ToList();
            for (var i = 0; i < igels.Count - 1; i++)
            {
                var pos = igels[i].nextIgelPosition;
                Assert.IsTrue(igelPos.ContainsKey(pos));
                var nextIgel = igelPos[pos];
                Assert.IsTrue(nextIgel.nextIgelPosition < 0 || nextIgel.nextIgelPosition > pos);
            }
        }

        [Test]
        public void TestCarrotsPerMove()
        {
            Assert.AreEqual(1, FieldMap.GetCarrotsPerMove(1));
            Assert.AreEqual(3, FieldMap.GetCarrotsPerMove(2));
            Assert.AreEqual(6, FieldMap.GetCarrotsPerMove(3));
            Assert.AreEqual(10, FieldMap.GetCarrotsPerMove(4));
            Assert.AreEqual(15, FieldMap.GetCarrotsPerMove(5));
            Assert.AreEqual(21, FieldMap.GetCarrotsPerMove(6));
            Assert.AreEqual(28, FieldMap.GetCarrotsPerMove(7));
            Assert.AreEqual(36, FieldMap.GetCarrotsPerMove(8));
            Assert.AreEqual(45, FieldMap.GetCarrotsPerMove(9));
            Assert.AreEqual(55, FieldMap.GetCarrotsPerMove(10));
        }

        [Test]
        public void TestMovesPerCarrots()
        {
            Assert.AreEqual(1, FieldMap.GetMovesForCarrots(1));
            Assert.AreEqual(4, FieldMap.GetMovesForCarrots(10));
            Assert.AreEqual(4, FieldMap.GetMovesForCarrots(13));
            Assert.AreEqual(10, FieldMap.GetMovesForCarrots(56));
        }
    }
}
