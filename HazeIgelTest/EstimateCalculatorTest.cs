﻿using HazeIgel.Model.AI;
using HazeIgel.Util;
using NUnit.Framework;

namespace HazeIgelTest
{
    [TestFixture]
    public class EstimateCalculatorTest : TurnCheckerBaseTest
    {
        public EstimateCalculatorTest()
        {
            ExecutablePath.InitializeFake(string.Empty);
        }

        [Test]
        public void TestComparingSalad()
        {
            // simply compare position
            var disp = MakeDisposition(0,
                SpawnHaze(45, 10, 0, false),
                SpawnHaze(44, 10, 0, false));
            var calc = new EstimateCalculator(map, disp);
            var p0 = calc.EstimatePosition(0);
            var p1 = calc.EstimatePosition(1);
            Assert.Greater(p0, p1);
        }

        [Test]
        public void TestEstimateFinish()
        {
            var disp = MakeDisposition(0,
                SpawnHaze(13, 357, 1, false),
                SpawnHaze(64, 5, 0, false));
            var calc = new EstimateCalculator(map, disp);
            var p0 = calc.EstimatePosition(0);
            var p1 = calc.EstimatePosition(1);
            Assert.Greater(p1, p0);
        }
    }
}
