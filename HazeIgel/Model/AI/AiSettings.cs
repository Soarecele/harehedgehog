﻿namespace HazeIgel.Model.AI
{
    /// <summary>
    /// settings for TurnMaker class
    /// </summary>
    public class AiSettings
    {
        /// <summary>
        /// max level of recursion
        /// 1: 0 moves, source disposition
        /// 2: N dispositions after the first move
        /// 3: ~ N * N dispositions after the second movie etc
        /// </summary>
        public int maxLevel = 8;

        /// <summary>
        /// take N best moves (by current level estimation) for each level
        /// and skip the others
        /// [0, 0, 8, 8, 6] means:
        /// - consider all the turns for the first and second moves
        /// - consider 8 "best" options for the 3d and 4th moves
        /// - consider 6 "best" options for the rest of the moves
        /// </summary>
        public int[] pruneRestrictionByLevel;

        /// <summary>
        /// is alpha-beta pruning enabled?
        /// </summary>
        public bool enablePruning = true;

        /// <summary>
        /// first try the "best"(by current estimation) moves
        /// for more effective pruning
        /// </summary>
        public bool enableSorting = true;

        /// <summary>
        /// how to decide which estimation is better? e.g.,
        /// [10, 20, 30] or [-10, 5, 2] for player[1]?
        /// - BestOfAll: [-10, 5, 2] is the better option
        /// - AbsoluteMaximum: [10, 20, 30] is the better option
        /// </summary>
        public EstimateContractingFun contractFunc;

        public override string ToString()
        {
            var rst = pruneRestrictionByLevel == null ? "nil" : 
                "[" + string.Join(", ", pruneRestrictionByLevel) + "]";
            return $"up to {maxLevel} level, {contractFunc}, pruning: {enablePruning}, sorting: {enableSorting}, restrictions: {rst}";
        }
    }

    public enum EstimateContractingFun
    {
        BestOfAll = 0, // professor Fate
        AbsoluteMaximum // great Leslie
    }
}
