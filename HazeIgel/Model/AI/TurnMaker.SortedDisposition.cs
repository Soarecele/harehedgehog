﻿namespace HazeIgel.Model.AI
{
    public partial class TurnMaker
    {
        class SortedDisposition
        {
            public Disposition disp;

            public int step;

            public int estimate;

            public int[] estimationVector;

            public SortedDisposition(Disposition disp, int step)
            {
                this.disp = disp;
                this.step = step;
            }

            public SortedDisposition(Disposition disp, int step, int estimate) : this(disp, step)
            {
                this.estimate = estimate;
            }
        }
    }
}
