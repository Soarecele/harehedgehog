﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HazeIgel.Model.AI
{
    /// <summary>
    /// this class builds and solves Minimax tree
    /// thus calculating the "best" of possible moves
    /// 
    /// class requires at least a disposition object
    /// </summary>
    public partial class TurnMaker
    {
        /// <summary>
        /// minimax tree solving params
        /// </summary>
        public AiSettings aiSettings = new AiSettings();

        /// <summary>
        /// initial (current) disposition, being passed in ctor()
        /// </summary>
        private Disposition initDisp;

        /// <summary>
        /// reference to a map, will stay untouched
        /// </summary>
        private FieldMap map;

        /// <summary>
        /// reference to field map's extra description
        /// </summary>
        private FieldMapDescriptor desc;

        /// <summary>
        /// one of the functions: ContractEstimateByRelativeNumber or
        /// ContractEstimateByAbsMax, depending on aiSettings,
        /// being set in CalculateTurn()
        /// </summary>
        private Func<int[], int, int> estimateContractingFunc;

        /// <summary>
        /// Minimax tree statistics, being used in design time for optimizations
        /// </summary>
        public readonly TurnMakerStatistics statistics = new TurnMakerStatistics();

        /// <summary>
        /// the resulted estimation vector, used for tests
        /// </summary>
        public int[] finalVector;        

        public TurnMaker(Disposition initDisp, FieldMap map, FieldMapDescriptor desc)
        {
            this.initDisp = initDisp;
            this.map = map;
            this.desc = desc;
        }

        /// <summary>
        /// main class's function, returns the "best" move code
        /// goes down in CalcEstimate()
        /// </summary>
        public int CalculateTurn()
        {
            estimateContractingFunc =
                aiSettings.contractFunc == EstimateContractingFun.BestOfAll
                    ? (Func<int[], int, int>)ContractEstimateByRelativeNumber
                    : ContractEstimateByAbsMax;
            var checker = new TurnChecker(map, initDisp, desc);
            var steps = checker.GetPossibleTurns();
            if (steps.Count == 1)
                return steps[0];

            var bestStep = steps[0];
            var bestEstimate = int.MinValue;

            int alpha = int.MinValue, beta = int.MaxValue;
            var sortedSteps = SortStepsByEstimating(initDisp, steps, 0);
            foreach (var step in sortedSteps)
            {
                var estimate = CalcEstimate(step, 2, alpha, beta);
                if (estimate == null)
                    continue;
                var contracted = estimateContractingFunc(estimate, initDisp.activeHaze);
                if (contracted <= bestEstimate)
                    continue;
                bestEstimate = contracted;
                bestStep = step.step;
                finalVector = step.estimationVector;
            }
            return bestStep;
        }

        /// <summary>
        /// check each step, calculate an estimation vector per each step
        /// using heuristic only (EstimateCalculator) and orders the
        /// steps by contracted (scalar) estimate in descending order
        /// </summary>
        /// <param name="level">current Minimax tree's level</param>
        private List<SortedDisposition> SortStepsByEstimating(Disposition disp, List<int> steps, int level)
        {
            statistics.estimations += steps.Count;
            statistics.nodes += steps.Count;
            var dispWithEstimate = new List<SortedDisposition>();
            foreach (var step in steps)
            {
                var checker = new TurnChecker(map, disp, desc)
                {
                    haseEffectKind = TurnChecker.HaseCellEffect.AiResolverEffect
                };
                var newDisp = checker.MakeTurnWoChecks(step);
                var calc = new EstimateCalculator(map, newDisp);
                var vector = newDisp.hazes.Select((h, i) => calc.EstimatePosition(i)).ToArray();
                var contracted = estimateContractingFunc(vector, disp.activeHaze);
                dispWithEstimate.Add(new SortedDisposition(newDisp, step, contracted)
                {
                    estimationVector = vector
                });
            }
            if (!aiSettings.enableSorting)
                return dispWithEstimate;
            var query = (IEnumerable<SortedDisposition>) dispWithEstimate.OrderByDescending(d => d.estimate);
            if (aiSettings.pruneRestrictionByLevel != null)
            {
                var maxCount = level >= aiSettings.pruneRestrictionByLevel.Length
                    ? aiSettings.pruneRestrictionByLevel.Last()
                    : aiSettings.pruneRestrictionByLevel[level];
                if (maxCount > 0)
                    query = query.Take(maxCount);
            }
            return query.ToList();
        }

        /// <summary>
        /// used recursively to go through (solve) the Minimax tree,
        /// also calls SortStepsByEstimating
        /// </summary>
        private int[] CalcEstimate(SortedDisposition disp, int turn, int alpha, int beta)
        {
            // if we are at a leaf level, estimate position
            //
            // also make estimation if our player just finished
            if (turn == aiSettings.maxLevel ||
                disp.disp.GetPreviousPlayerIndex() == initDisp.activeHaze &&
                disp.disp.hazes[initDisp.activeHaze].Finished)
                return disp.estimationVector;

            // else go deeper and sort out the results
            var checker = new TurnChecker(map, disp.disp, desc);
            var steps = checker.GetPossibleTurns();
            var sortedSteps = SortStepsByEstimating(disp.disp, steps, turn - 1);

            var bestEstimation = int.MinValue;
            int[] bestVector = null;            

            foreach (var step in sortedSteps)
            {
                var estimationVector = CalcEstimate(step, turn + 1, alpha, beta);
                if (estimationVector == null)
                    continue;
                var estimation = estimateContractingFunc(estimationVector, disp.disp.activeHaze);
                if (estimation <= bestEstimation)
                    continue;
                bestEstimation = estimation;
                bestVector = estimationVector;

                // alpha-beta-gamma-... pruning
                var aiEstim = disp.disp.activeHaze == 0 ? estimation : estimateContractingFunc(estimationVector, 0);
                if (disp.disp.activeHaze == 0)
                {
                    if (aiEstim > alpha)
                        alpha = aiEstim;
                }                
                else 
                    if (aiEstim < beta)
                        beta = aiEstim;
                if (aiSettings.enablePruning)
                    if (beta <= alpha)
                        break;                
            }
            return bestVector;
        }

        /// <summary>
        /// make a scalar estimation ("best" for player[i]) out of vector estimation
        /// [player0, player1, ..., playerN]
        /// 
        /// used when aiSettings.contractFunc.BestOfAll is chosen
        /// </summary>
        private int ContractEstimateByRelativeNumber(int[]eVector, int player)
        {
            int? min = null;
            var pVal = eVector[player];
            for (var i = 0; i < eVector.Length; i++)
            {
                if (i == player) continue;
                var val = pVal - eVector[i];
                if (!min.HasValue || min.Value > val)
                    min = val;
            }
            return min.Value;
        }

        /// <summary>
        /// make a scalar estimation ("best" for player[i]) out of vector estimation
        /// [player0, player1, ..., playerN] - just returns [i]-value
        /// 
        /// used when aiSettings.contractFunc.AbsoluteMaximum is chosen
        /// </summary>
        private int ContractEstimateByAbsMax(int[] eVector, int player)
        {
            return eVector[player];
        }
    }
}
