﻿namespace HazeIgel.Model.AI
{
    /// <summary>
    /// statistics gathered while searching for the "best" move
    /// </summary>
    public class TurnMakerStatistics
    {
        /// <summary>
        /// how many times a disposition was estimated
        /// (not actual, because each disposition is to be evaluated
        /// for the current implementation)
        /// </summary>
        public int estimations;

        /// <summary>
        /// total nodes in Minimax trees
        /// </summary>
        public int nodes;

        public override string ToString()
        {
            return $"{estimations} estimations, {nodes} nodes total";
        }
    }
}
