﻿namespace HazeIgel.Model
{
    /// <summary>
    /// a "move" is coded by a simple number:
    ///  0 to stay on the same cell,
    ///  1, 2, .. N to step up to N cells forward,
    ///  -1, -2, ... -M to step up to M cells backward (on Igel)
    ///  1001 to take 10 carrots, 1002 to give out 10 carrots
    /// 
    /// the newState field contains the number of carrots and salad card
    /// after making this move
    /// 
    /// finished flag shows is player on the Finish cell after the move
    /// 
    /// the class is used in GUI to make a human some hint on what changes after his move
    /// </summary>
    public class ExplainedStep
    {
        public int command;        

        public Haze newState;

        public bool finished;

        public ExplainedStep()
        {
        }

        public ExplainedStep(int command)
        {
            this.command = command;
        }

        public override string ToString()
        {
            var stepStr = command == TurnChecker.CmdTakeCarrots
                ? "take carrots"
                : command == TurnChecker.CmdGiveCarrots
                    ? "give carrots"
                    : command > 0
                        ? $"step from {newState.pos - command} to {newState.pos}"
                        : command == 0
                            ? "wait"
                            : $"leap back on Igel at {newState.pos}";
            var finishedStr = finished ? ", finished" : "";
            return $"{stepStr}, {newState.carrots} carrots, {newState.salad} salad{finishedStr}";
        }
    }
}
