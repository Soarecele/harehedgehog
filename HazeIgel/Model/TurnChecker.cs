﻿using System;
using System.Collections.Generic;
using System.Linq;
using HazeIgel.Model.AI; // for cref

namespace HazeIgel.Model
{
    /// <summary>
    /// this class:
    ///  - enumerates the allowed moves (turns or steps) for the 
    ///    current position (disp) (GetPossibleTurns or GetExplainedSteps)
    ///  
    ///  - performs the provided move (cmd, methods CheckTurnAndUpdate or MakeTurnWoChecks)
    ///    and makes the next disposition (newDisp) out of the present one (disp)
    /// </summary>
    public partial class TurnChecker
    {
        /// <summary>
        /// map instance, remains unchanged
        /// </summary>
        private FieldMap map;

        /// <summary>
        /// extra map info
        /// TODO: merge with FieldMap class
        /// </summary>
        private FieldMapDescriptor mapDescriptor;

        /// <summary>
        /// present disposition
        /// </summary>
        private Disposition disp;

        /// <summary>
        /// next disposition, made of the present one after making the move,
        /// provided by the command (cmd)
        /// </summary>
        private Disposition newDisp;

        /// <summary>
        /// player, who's making his move in present (disp) disposition
        /// </summary>
        private Haze haze;

        /// <summary>
        /// the same player (haze), but after making his move (in the new disposition)
        /// </summary>
        private Haze newHaze;

        /// <summary>
        /// command, -M ... 0 .. N, [1001 or 1002]
        /// <see cref="ExplainedStep"/>
        /// </summary>
        private int cmd;

        /// <summary>
        /// how much carrots the player will have after his move
        /// </summary>
        private int carrotsAfterMove;

        /// <summary>
        /// used for Hase effects
        /// </summary>
        private static readonly Random random = new Random();

        /// <summary>
        /// true if MakeTurnWoChecks called
        /// </summary>
        private bool skipChecks;

        public TurnChecker(FieldMap map, Disposition disp, FieldMapDescriptor mapDescriptor)
        {
            this.map = map;
            this.mapDescriptor = mapDescriptor;
            this.disp = disp;
            newDisp = disp.Clone();
            newHaze = newDisp.hazes[disp.activeHaze];
            haze = disp.ActiveHaze;
        }

        public TurnChecker(FieldMap map, Disposition disp) : this(map, disp, new FieldMapDescriptor(map))
        {
        }

        /// <summary>
        /// - check if the provided move (cmd) is allowed
        /// - make the provided move and build a new disposition (newDisp)
        /// 
        /// used when a player is making his turn
        /// when the AI is making his turn MakeTurnWoChecks is used instead
        /// </summary>
        /// <param name="cmd"><see cref="ExplainedStep"/></param>
        /// <returns>newDisp</returns>
        public Disposition CheckTurnAndUpdate(int cmd)
        {
            this.cmd = cmd;
            var result = CheckTurn();
            result?.UpdatePositions(map);
            return result;
        }

        /// <summary>
        /// just makes the provided move
        /// checks are unnecessary because the AI couldn't have mistaken
        /// 
        /// also used in <see cref="TurnMaker"/>
        /// </summary>
        public Disposition MakeTurnWoChecks(int cmd)
        {
            this.cmd = cmd;
            skipChecks = true;
            var result = CheckTurn();
            result?.UpdatePositions(map);
            return result;
        }

        /// <summary>
        /// make a list of possible moves for the current disposition,
        /// for the player who's taking his step
        /// <see cref="ExplainedStep"/>
        /// </summary>
        public List<int> GetPossibleTurns()
        {
            if (haze.waits || haze.Finished)
                return new List<int> { 0 };
            var turns = new List<int>();

            CalcCarrotsAfterMove(0);
            var resultedCarrots = carrotsAfterMove;

            // try N leaps forward
            var maxMovesForward = FieldMap.GetMovesForCarrots(resultedCarrots);
            var lastCell = Math.Min(haze.pos + maxMovesForward, map.fieldsCount - 1);
            for (var i = haze.pos + 1; i <= lastCell; i++)
            {
                var steps = i - haze.pos;
                carrotsAfterMove = resultedCarrots - FieldMap.GetCarrotsPerMove(steps);
                if (CanMoveForwardOrFinish(i))
                    turns.Add(steps);
            }

            // staying on carrot
            if (map.cells[haze.pos] is CarrotCell)
            {
                turns.Add(CmdTakeCarrots);
                if (resultedCarrots >= CarrotsPerWaiting)
                    turns.Add(CmdGiveCarrots);
            }

            // leap back on Igel
            var igelPos = mapDescriptor.nearestIgelByPos[haze.pos];
            if (igelPos > 0 && !disp.occupiedPos[igelPos])
                turns.Add(igelPos - haze.pos);

            if (turns.Count == 0)
                turns.Add(0);
            return turns;
        }

        /// <summary>
        /// build upon GetPossibleTurns, returns some explanation
        /// per each step
        /// </summary>
        public List<ExplainedStep> GetExplainedSteps()
        {
            var steps = GetPossibleTurns().Select(t => new ExplainedStep(t)).ToList();
            foreach (var step in steps)
            {
                var turn = new TurnChecker(map, disp, mapDescriptor)
                {
                    haseEffectKind = HaseCellEffect.NoEffect
                }.CheckTurnAndUpdate(step.command);
                step.newState = turn.hazes[disp.activeHaze];
                step.finished = turn.hazes[disp.activeHaze].pos == map.fieldsCount - 1;
            }
            return steps;
        }

        /// <summary>
        /// here's all the logic for CheckTurnAndUpdate and
        ///  MakeTurnWoChecks methods 
        /// </summary>
        private Disposition CheckTurn()
        {
            // just wait
            if (cmd == 0)
                return CheckWaitCmdOk() ? newDisp : null;

            // leap back on un Igel
            if (cmd < 0)
                return CheckLeapBackCmdOk() ? newDisp : null;

            // give or take some carrots
            if (cmd == CmdTakeCarrots || cmd == CmdGiveCarrots)
                return GiveOrTakeCarrots() ? newDisp : null;

            // usual step forward
            if (haze.pos + cmd >= map.fieldsCount)
                return null;

            CalcCarrotsAfterMove(cmd);
            if (carrotsAfterMove < 0)
                return null;

            if (!skipChecks)
                if (!CanMoveForwardOrFinish(haze.pos + cmd))
                    return null;
            
            UpdateCarrotsAndPosAfterMove();
            return newDisp;
        }

        /// <summary>
        /// update player carrots and salad after making a move forward
        /// </summary>
        private void UpdateCarrotsAndPosAfterMove()
        {
            newHaze.carrots = carrotsAfterMove;
            newHaze.pos += cmd;
            if (map.cells[newHaze.pos] is SaladCell)
                newHaze.waits = true;
            else if (map.cells[newHaze.pos] is HaseCell)
                ApplyHaseEffect();
            else if (newHaze.pos == map.fieldsCount - 1)
            {
                // set haze index at which he finished
                newHaze.index = newDisp.hazes.Max(h => h.index) + 1;
            }
        }

        /// <summary>
        /// give or take carrots while staying on carrot cell
        /// </summary>
        private bool GiveOrTakeCarrots()
        {
            if (map.cells[haze.pos] is CarrotCell == false) return false;
            if (cmd == CmdTakeCarrots)
            {
                newHaze.carrots += CarrotsPerWaiting;
                return true;
            }
            if (newHaze.carrots < CarrotsPerWaiting) return false;
            newHaze.carrots -= CarrotsPerWaiting;
            return true;
        }

        /// <summary>
        /// can player make a move forward (on newPos cell), whether he is finishing or not
        /// </summary>
        private bool CanMoveForwardOrFinish(int newPos)
        {
            if (newPos == map.fieldsCount - 1)
            {
                if (!CanFinish()) return false;
            }
            else
            {
                if (!CanMoveForward(newPos)) return false;
            }
            return true;
        }

        /// <summary>
        /// used in CanMoveForwardOrFinish
        /// </summary>
        private bool CanMoveForward(int newPos)
        {
            if (disp.occupiedPos[newPos]) return false;
            var cell = map.cells[newPos];
            if (cell is IgelCell) return false;
            if (cell is SaladCell && haze.salad == 0) return false;
            return true;
        }

        /// <summary>
        /// used in CanMoveForwardOrFinish
        /// </summary>
        private bool CanFinish()
        {
            if (haze.salad > 0) return false;
            var countFinished = disp.hazes.Count(h => h.pos == map.fieldsCount - 1);
            var maxAllowed = (countFinished + 1) * CarrotsMaxOnFinish;
            return carrotsAfterMove <= maxAllowed;
        }

        /// <summary>
        /// how much carrots will the player have after a move forward
        /// take into account position or salad cell's effects
        /// </summary>
        private void CalcCarrotsAfterMove(int command)
        {
            var deltaC = 0;
            
            // carrots taken for haze's position
            if (map.cells[haze.pos] is PositionCell posCell)
            {
                if (posCell.OnRightPosition(disp.hazePos))
                    deltaC += CarrotsPerPosition * disp.hazePos;
            }

            // carrots taken if leaving salad
            if (map.cells[haze.pos] is SaladCell)            
                deltaC += CarrotsPerPosition * disp.hazePos;

            // carrots required for the step
            deltaC -= FieldMap.GetCarrotsPerMove(command);
            carrotsAfterMove = haze.carrots + deltaC;
        }

        /// <summary>
        /// can leap back on Igel cell
        /// </summary>
        private bool CheckLeapBackCmdOk()
        {
            var newPos = haze.pos + cmd;
            if (!skipChecks)
            {
                if (newPos < 0) return false;
                if (map.cells[newPos] is IgelCell == false)
                    return false;

                // is the Igel nearest one
                var igCell = (IgelCell) map.cells[newPos];
                if (igCell.nextIgelPosition > 0 && igCell.nextIgelPosition < haze.pos)
                    return false;

                // cell is blocked
                if (disp.occupiedPos[newPos]) return false;
            }

            // take some carrots
            newHaze.carrots -= CarrotsPerLeapBack * cmd;
            newHaze.pos = newPos;
            return true;
        }

        /// <summary>
        /// can stay on the current cell
        /// </summary>
        private bool CheckWaitCmdOk()
        {
            if (!skipChecks)
                if (!haze.Finished && !haze.waits)
                {
                    // should wait if can't go forward or backward
                    return IfHasToWait();
                }
            // spare salad (if he didn't HAS to wait)
            if (map.cells[haze.pos] is SaladCell && haze.waits)
                newHaze.salad--;
            newHaze.waits = false;

            return true;
        }

        /// <summary>
        /// check if the player doesn't have another option but stay on the current cell
        /// </summary>
        private bool IfHasToWait()
        {
            if (map.cells[haze.pos] is CarrotCell) return false;

            CalcCarrotsAfterMove(cmd);
            if (carrotsAfterMove > 0)
            {
                var stepsCount = FieldMap.GetMovesForCarrots(carrotsAfterMove);
                var nextPos = Math.Min(haze.pos + stepsCount, map.fieldsCount - 1);
                for (var i = haze.pos + 1; i <= nextPos; i++)
                {
                    // can occupy this cell?
                    if (CanMoveForwardOrFinish(i)) return false;
                }
            }

            var hazePos = mapDescriptor.nearestIgelByPos[haze.pos];
            if (hazePos < 0) return false;
            return disp.occupiedPos[hazePos];
        }
    }
}