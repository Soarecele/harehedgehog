﻿using System;
using System.Collections.Generic;
using HazeIgel.Util;
using Newtonsoft.Json;

namespace HazeIgel.Model.Game
{
    /// <summary>
    /// REST API base command, that can be one of 5 commands
    /// stores class name in ClassName field
    /// </summary>
    public class GameCommand : JsonTypedObjectEx
    {
        private static readonly Dictionary<string, Type> _typeByName = new Dictionary<string, Type>
        {
            { nameof(GameCommandAiTurn), typeof(GameCommandAiTurn) },
            { nameof(GameCommandNewGame), typeof(GameCommandNewGame) },
            { nameof(GameCommandPlayerTurn), typeof(GameCommandPlayerTurn) },
            { nameof(GameCommandLoadSample), typeof(GameCommandLoadSample) },
            { nameof(GameCommandUpdateSample), typeof(GameCommandUpdateSample) }
        };

        private static readonly JsonSerializerSettings _deserzSets = new JsonSerializerSettings
        {
            ObjectCreationHandling = ObjectCreationHandling.Replace
        };

        public static GameCommand ParseCommand(string json)
        {
            if (string.IsNullOrEmpty(json)) return null;
            return Deserialize(json, _typeByName, _deserzSets) as GameCommand;
        }

        public virtual void AfterParse()
        {
        }
    }
}
