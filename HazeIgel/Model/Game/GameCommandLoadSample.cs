﻿namespace HazeIgel.Model.Game
{
    /// <summary>
    /// load a sample disposition from EstimationStore
    /// used when solving "etudes"
    /// </summary>
    public class GameCommandLoadSample : GameCommand
    {
        /// <summary>
        /// sample's index
        /// </summary>
        public int sample;

        public GameCommandLoadSample()
        {
            ClassName = "GameCommandLoadSample";
        }        
    }
}
