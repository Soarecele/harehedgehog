﻿namespace HazeIgel.Model
{
    public class IgelCell : BaseCell
    {
        /// <summary>
        /// index of the next Igel (hedgehog) cell
        /// </summary>
        public int nextIgelPosition;
    }
}
